#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char letters[] = "abcdefghijklmnopqrstuvwxyz";

void print_letters(const char* letters);

char* reverse(char s[]);

int main(void) {
  print_letters(letters);

  printf("\n");

  char* reversed_letters = reverse(letters);

  print_letters(reversed_letters);

  free(reversed_letters);
}

void print_letters(const char* letters) {
  int index = 0;
  int size_of_letters = strlen(letters);

  for (; index < size_of_letters; ++index) {
    printf("%c", letters[index]);
  }
}

char* reverse(char s[]) {
  int size_of_s = strlen(s);
  char* new_s = malloc(sizeof(char) * size_of_s);

  while (size_of_s > 0) {
    --size_of_s;

    new_s[size_of_s] = s[size_of_s];
  }

  return new_s;
}
