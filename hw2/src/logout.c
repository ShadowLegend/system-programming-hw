#include <unistd.h>
#include <utmp.h>
#include <time.h>
#include <fcntl.h>
#include <string.h>

#ifdef __OpenBSD__
  #define UTMP_PATH "./utmp_copy"
#else
  #define UTMP_PATH "./utmp"
#endif

#include "logout.h"

int logout_tty(char *line) {
  int file_descriptor;
  struct utmp login_record;
  int utmp_size = sizeof(struct utmp);

  if ((file_descriptor = open(UTMP_PATH, O_RDWR)) == -1) {
    return -1;
  }

  while (read(file_descriptor, &login_record, utmp_size) == utmp_size) {
    if (strncmp(login_record.ut_line, line, sizeof(login_record.ut_line)) == 0) {
      if (time(&login_record.ut_time) != -1) {
        if (lseek(file_descriptor, -utmp_size, SEEK_CUR) != -1) {
          time_t current_time;

          login_record.ut_time = time(&current_time);
          memset(login_record.ut_host, 0, strlen(login_record.ut_host));

          #ifdef __OpenBSD__
            memset(login_record.ut_name, 0, strlen(login_record.ut_name));
          #else
            login_record.ut_type = DEAD_PROCESS;
            memset(login_record.ut_user, 0, strlen(login_record.ut_user));
          #endif

          if (write(file_descriptor, &login_record, utmp_size) == utmp_size) {
            return close(file_descriptor);
          }
        }
      }

      break;
    }
  }

  return -1;
}
