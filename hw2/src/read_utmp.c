#include <fcntl.h>
#include <unistd.h>
#include <utmp.h>
#include <stdio.h>
#include <string.h>

#include "read_utmp.h"

int read_utmp(const char *path_to_utmp) {
  int file_descriptor = open(path_to_utmp, O_RDONLY);

  if (file_descriptor != -1) {
    struct utmp login_record;
    int utmp_size = sizeof(struct utmp);

    while (read(file_descriptor, &login_record, utmp_size) == utmp_size) {
      // write(STDOUT_FILENO, login_record.ut_user, strlen(login_record.ut_user));
      #ifdef __OpenBSD__
        printf("%s\n", login_record.ut_name);
      #else
        printf("%s\n", login_record.ut_user);
      #endif
    }

    return close(file_descriptor);
  }

  return 1;
}
