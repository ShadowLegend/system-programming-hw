#include <stdio.h>

#ifdef __OpenBSD__
  #define UTMP_PATH "./utmp_copy"
  #define TTY_TO_REMOVE "ttyp0"
#else
  #define UTMP_PATH "./utmp"
  #define TTY_TO_REMOVE "tty1"
#endif

#include "read_utmp.h"
#include "logout.h"

int main(void) {
  // before modified
  read_utmp(UTMP_PATH);

  // using logout_tty from molay's book
  // from section 2.9.4 with little modification
  logout_tty(TTY_TO_REMOVE);

  // after modified
  read_utmp(UTMP_PATH);

  return 0;
}
