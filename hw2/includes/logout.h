#ifndef LOGOUT_H
#define LOGOUT_H

int logout_tty(char *line);
int logout_tty2(const char *path_to_utmp, const char *line);

#endif