// LAB 2
// EMSAMBORAMY VONG & HENG LY
#include <stdio.h>

void read(FILE *file);

int main(int args_len, char ** args_context) {
  FILE* file = NULL;

  if (args_len > 1) {
    ++args_context;

    while (*args_context) {
      file = fopen(*args_context, "r");

      if (file) {
        read(file);
        fclose(file);
      } else {
        printf("cat: %s: No such file or directory\n", *args_context);
      }

      ++args_context;
    }
  } else {
    int int_character;

    while ((int_character = getchar()) != EOF) {
      printf("%c", int_character);
    }
  }

  return 0;
}

void read(FILE *file) {
  int int_character;

  while ((int_character = fgetc(file)) != EOF) {
    putchar(int_character);
  }
}
